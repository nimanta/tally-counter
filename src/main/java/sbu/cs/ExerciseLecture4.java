package sbu.cs;

import java.util.Objects;

public class ExerciseLecture4 {

    public long factorial(int n) {

        if (n == 0)
            return 1;
        else
            return n * factorial(n - 1);
    }

    public long fibonacci(int n) {

        if (n == 1)
            return 1;
        else if (n == 2)
            return 1;

        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    public String reverse(String word) {

        StringBuilder s = new StringBuilder(word).reverse();

        return s.toString() ;
    }

    public boolean isPalindrome(String line) {

        return new StringBuilder(line).reverse().toString().replace(" ", "")
                                                .equalsIgnoreCase(line.replace(" ", ""));
    }

    public char[][] dotPlot(String str1, String str2) {
        char[][] dp = new char[str1.length()][str2.length()];

        char[] s1 = str1.toCharArray();
        char[] s2 = str2.toCharArray();

        for (int i = 0; i < s1.length; i++)
            for (int j = 0; j < s2.length; j++)
                if (s1[i] == s2[j])
                    dp[i][j] = '*';
                else
                    dp[i][j] = ' ';

        return dp;
    }
}
