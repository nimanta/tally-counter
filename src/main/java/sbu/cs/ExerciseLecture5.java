package sbu.cs;

import java.security.SecureRandom;

public class ExerciseLecture5 {

    public SecureRandom random = new SecureRandom();

    public String weakPassword(int length) {

        StringBuilder weak = new StringBuilder();

        for (int i = 0; i < length; i++) {
            weak.append((char)(random.nextInt(26) + 97));
        }

        return weak.toString();
    }

    public String strongPassword(int length) throws Exception {

        if (length < 3)
            throw new Exception();


        StringBuilder sp = new StringBuilder();

        char[] alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();
        char[] digit = "0123456789".toCharArray();
        char[] special = "~`!@#$%^&*()-_=+[{]}\\\\|;:\\'\\\",<.>/?".toCharArray();

        int digits = random.nextInt(10);
        sp.append(digit[digits]);
        System.out.println(sp.toString());

        int index;
        index = random.nextInt(52);
        sp.append(alpha[index]);
        System.out.println(sp.toString());

        index = random.nextInt(special.length);
        sp.append(special[index]);

        for (int i = 3; i < length ; i++) {
            int state = random.nextInt(3);
            if (state == 0) {
                index = random.nextInt(52);
                sp.append((char) index);
            } else if (state==1) {
                index = random.nextInt(10);
                sp.append(digit[index]);
            } else {
                index = random.nextInt(special.length);
                sp.append(special[index]);
            }
        }
        System.out.println(sp.toString());

        char[] characters = sp.toString().toCharArray();
        for (int i = 0; i < characters.length; i++) {
            int j = random.nextInt(characters.length);
            char temp = characters[i];
            characters[i] = characters[j];
            characters[j] = temp;
        }
        System.out.println(new String(characters));
        return new String(characters);
    }

    public boolean isFiboBin(int n) {
        int i = 1;

        do {
            if (fib(i) + Integer.toBinaryString(fib(i)).replace("0","").length() == n)
                return true;
            if (fib(i) + Integer.toBinaryString(fib(i)).replace("0","").length() > n)
                return false;
            i++;
        }while(true);
    }

    public int fib (int i) {
        if (i == 0)
            return 0;
        if (i == 1)
            return 1;
        return fib(i - 1) + fib(i - 2);
    }
}
