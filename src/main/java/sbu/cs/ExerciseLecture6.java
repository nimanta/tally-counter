package sbu.cs;

import java.util.ArrayList;
import java.util.List;

public class ExerciseLecture6 {
    
    public long calculateEvenSum(int[] arr) {

        long sum = 0; 
        for (int i = 0; i < arr.length; i += 2) {
            sum += arr[i];
        }
        return sum;
    }
    
    public int[] reverseArray(int[] arr) {

        int[] reverse = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            reverse[arr.length - i - 1] = arr[i];
        }
        return reverse;
    }

    
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {

        if (m1[0].length != m2.length)
            throw new RuntimeException();
        
        double[][] product = new double[m1.length][m2[0].length];

        for (int i = 0; i < m1.length; i++)
            for (int j = 0; j < m2[0].length; j++) {
                product[i][j] = 0;
                for (int k = 0; k < m2.length; k++)
                    product[i][j] += m1[i][k] * m2[k][j];
            }

        return product;
    }
    
    public List<List<String>> arrayToList(String[][] names) {

        List<List<String>> converted = new ArrayList<>();

        for (int i = 0; i < names.length; i++){
            List<String> rows = new ArrayList<>();
            for (int j = 0; j < names[i].length; j++)
                rows.add(names[i][j]);

            converted.add(rows);
        }

        return converted;
    }
    
    public List<Integer> primeFactors(int n) {

        List<Integer> primeFactors = new ArrayList<>();

        if (n % 2 == 0) {
            while (n % 2 == 0) {
                n /= 2;
            }
            primeFactors.add(2);
        }

        for (int i = 3; i <= Math.sqrt(n); i+= 2)
        {
            if (n % i == 0) {
                while (n % i == 0) {
                    n /= i;
                }
                primeFactors.add(i);
            }
        }

        if (n > 2)
            primeFactors.add(n);

        return primeFactors;
    }

    public List<String> extractWord(String line) {

        List<String> extractWords = new ArrayList<>();

        line = line.trim();

        String nonWord = "\\W+";
        String[] splited = line.split(nonWord);

        for (String item : splited) {
            extractWords.add(item);
        }

        return extractWords;
    }
}
