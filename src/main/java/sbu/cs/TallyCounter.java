package sbu.cs;

public class TallyCounter implements TallyCounterInterface {

    private static int val;

    @Override
    public void count() {
        if (val <= 9998)
            val++;
    }

    @Override
    public int getValue() {

        return val;
    }

    @Override
    public void setValue(int value) throws IllegalValueException {
        if (value < 0 || value > 9999)
            throw new IllegalValueException();

        val = value;
    }
}
